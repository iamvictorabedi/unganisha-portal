# UnganishaPortal

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Getting Started

Install the necessary packages by running

```
npm install
```


The following information is obtained from the facebook developers site. for the app you will create using the following [steps](https://developers.facebook.com/docs/messenger-platform/guides/quick-start)

### Prerequisites

Npm should be running on your computer since it is a node js project


## Deployment

Install ziet to you machine via terminal

Run
```
$ npm install -g now
```

To deploy, head to your project directory and run
```
$ now
```

## Acknowledgments

Unganisha Portal, created by [Victor Abedi](https://iamvictorabedi@bitbucket.org/)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
